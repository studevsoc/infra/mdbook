FROM rust:latest
MAINTAINER Abraham Raji <avronr@tuta.io>

RUN cargo install mdbook

CMD ["/usr/local/bin/mdbook"]

WORKDIR /data
VOLUME /data

EXPOSE 3000

CMD /usr/local/bin/mdbook serve -n 0.0.0.0
